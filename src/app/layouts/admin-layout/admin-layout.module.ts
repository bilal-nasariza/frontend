import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { AnalyticsComponent } from '../../analytics/analytics.component';
import { TwitterComponent } from '../../analytics/twitter/twitter.component';
import { FacebookComponent } from '../../analytics/facebook/facebook.component';
import { KompasComponent } from '../../analytics/kompas/kompas.component';
import { OverviewComponent } from '../../overview/overview.component';
import { TwitterOverviewComponent } from '../../overview/twitter-overview/twitter-overview.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { DetikComponent } from  '../../analytics/detik/detik.component';

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule
} from '@angular/material';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    ReactiveFormsModule,
    HighchartsChartModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    AnalyticsComponent,
    TwitterComponent,
    FacebookComponent,
    OverviewComponent,
    TwitterOverviewComponent,
    KompasComponent,
    DetikComponent
  ]
})

export class AdminLayoutModule {}

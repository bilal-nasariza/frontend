import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError as ObservableThrowError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { TwitterAccount } from './models';

const api = 'http://localhost:5000';

@Injectable({
  providedIn: 'root'
})
export class OverviewService {

  constructor(private http: HttpClient) { }

  getTwittertAccounts() {
    return this.http.get<TwitterAccount[]>(`${api}/analytics/tw/accounts`)
      .pipe(
        tap(() => console.log("get twitter_accounts"))
      );
  }
}

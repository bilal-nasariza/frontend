import { Component, OnInit } from '@angular/core';
import { TwitterAccount, FbAccount } from './models';
import { OverviewService } from './overview.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  twitter_accounts: TwitterAccount[];
  fb_accounts: FbAccount[];
  loading: boolean;
  constructor(private overviewService: OverviewService) { }

  ngOnInit() {
    this.loading = true;
    this.overviewService.getTwittertAccounts()
    .pipe(finalize(() => (this.loading = false)))
    .subscribe(accounts => (
      this.twitter_accounts = accounts
    ));
  }

}

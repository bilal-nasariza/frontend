import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TwitterOverviewService } from './twitter-overview.service';
import { TwitterOverview } from './overview_models';
import { finalize } from 'rxjs/operators';
import * as Chartist from 'chartist';
import { analyzeAndValidateNgModules } from '@angular/compiler';
// import { ChartistTooltip } from 'chartist-plugin-tooltips-updated';
import ChartistTooltip from 'chartist-plugin-tooltips-updated';
import * as Highcharts from 'highcharts';
import * as moment from 'moment';

@Component({
  selector: 'app-twitter-overview',
  templateUrl: './twitter-overview.component.html',
  styleUrls: ['./twitter-overview.component.scss']
})
export class TwitterOverviewComponent implements OnInit {

  twitter_id: number;
  private sub: any;
  twitter_overview: TwitterOverview;
  loading: boolean;
  dataTotalFollowersChart: any;
  total_followers: number;
  follower_growth: number;
  total_listed: number;
  listed_growth: number;

  Highcharts = Highcharts;

  followerChartOptions = {
    title: "Total Followers",
    series: [
      {
        name: "followers",
        data: []
      }
    ],
    chart: {
      backgroundColor: null
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: null,
      labels: {
        formatter: function(y_axis) {
          // console.log(y_axis.value);
          return (y_axis.value / 1000) + 'k'
        }
      }
    }
  };

  followerGrowthChartOptions = {
    title: "Total Followers",
    series: [
      {
        name: "followers",
        data: []
      }
    ],
    chart: {
      backgroundColor: null,
      type: 'column'
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: null,
      labels: {
        formatter: function(y_axis) {
          // console.log(y_axis.value);
          return (y_axis.value / 1000) + 'k'
        }
      }
    }
  };

  totalListedChartOptions = {
    series: [{
      name: "Total Listed",
      data: []
    }],
    title: "Total Listed",
    chart: {
      backgroundColor: null,
      type: 'line'
    },
    xAxis: {
      categories: [],
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    yAxis: {
      title: null,
      labels: {
        formatter: function(y_axis) {
          // console.log(y_axis.value);
          if (y_axis.value > 1000) {
            return (y_axis.value / 1000) + 'k'
          } else {
            return y_axis.value
          }
        },
        style: {
          color: 'white'
        }
      }
    }
  };

  totalListedGrowthChartOptions = {
    title: "Total Followers",
    series: [
      {
        name: "followers",
        data: []
      }
    ],
    chart: {
      backgroundColor: null,
      type: 'column'
    },
    xAxis: {
      categories: [],
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    yAxis: {
      title: null,
      labels: {
        formatter: (y_axis) => {
          if (y_axis.value > 1000) {
            return (y_axis.value / 1000) + 'k'
          } else {
            return y_axis.value
          }
        },
        style: {
          color: 'white'
        }
      }
    }
  };

  numberOfProfileTweetChartOptions = {
    title: "Number Of Profile Tweet",
    series: [
      {
        name: "Number Of Profile Tweet",
        data: []
      }
    ],
    chart: {
      backgroundColor: null,
      type: 'column'
    },
    xAxis: {
      categories: [],
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    yAxis: {
      title: null,
      labels: {
        formatter: (y_axis) => {
          if (y_axis.value > 1000) {
            return (y_axis.value / 1000) + 'k'
          } else {
            return y_axis.value
          }
        },
        style: {
          color: 'white'
        }
      }
    }
  };

  profileActivitiesChartOptions = {
    title: "Profile Activities",
    series: [
      {
        name: "Tweets",
        data: []
      },
      {
        name: "Replies",
        data: []
      },
      {
        name: "Retweets",
        data: []
      }
    ],
    chart: {
      backgroundColor: null,
      type: 'column'
    },
    xAxis: {
      categories: [],
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    yAxis: {
      title: null,
      min: 0,
      stackLabels: {
        enabled: true
      },
      labels: {
        formatter: (y_axis) => {
          if (y_axis.value > 1000) {
            return (y_axis.value / 1000) + 'k'
          } else {
            return y_axis.value
          }
        },
        style: {
          color: 'white'
        }
      }
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
            enabled: true,
            color: 'white'
        }
      }
    }
  };

  engagementRateChartOptions = {
    title: "Engagement Rate",
    series: [
      {
        name: "Engagement Rate",
        data: []
      }
    ],
    chart: {
      backgroundColor: null,
      type: 'column'
    },
    xAxis: {
      categories: [],
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    yAxis: {
      title: null,
      labels: {
        formatter: (y_axis) => {
          if (y_axis.value > 1000) {
            return (y_axis.value / 1000) + 'k'
          } else {
            return y_axis.value
          }
        },
        style: {
          color: 'white'
        }
      }
    },
    tooltip: {
      pointFormat: "Engagement Rate {point.y:.2f}"
    }
  };

  constructor(private route: ActivatedRoute, private twitterOverviewService: TwitterOverviewService) { }

  ngOnInit() {
    this.total_followers = 0;
    this.sub = this.route.params.subscribe(params => {
      this.twitter_id = +params['id'];
      this.getProfileOverview(this.twitter_id);
    })
  }

  startAnimationForBarChart(chart){
    let seq2: any, delays2: any, durations2: any;

    seq2 = 0;
    delays2 = 80;
    durations2 = 500;
    chart.on('draw', function(data) {
      if(data.type === 'bar'){
          seq2++;
          data.element.animate({
            opacity: {
              begin: seq2 * delays2,
              dur: durations2,
              from: 0,
              to: 1,
              easing: 'ease'
            }
          });
      }
    });

    seq2 = 0;
};

  getProfileOverview(id: number) {
    this.loading = true;
    // console.log(this.twitterOverviewService);
    this.twitterOverviewService.getTwitterOverview(id)
    .pipe(finalize(() => (this.loading = false)))
    .subscribe((profile_overview) => {
      this.twitter_overview = profile_overview;
      this.total_followers = profile_overview.follower[profile_overview.follower.length - 1]['total_followers'];
      this.total_listed = profile_overview.follower[profile_overview.follower.length - 1]['total_listed'];
      console.log(profile_overview.follower)
      if (profile_overview.follower.length <= 1) {
        this.follower_growth = 0
        this.listed_growth = 0
      } else {
        this.follower_growth = this.total_followers - profile_overview.follower[profile_overview.follower.length - 2]['total_followers'];
      this.listed_growth = this.total_listed - profile_overview.follower[profile_overview.follower.length - 2]['total_listed'];
      }
      this.drawEngagementRateChart(profile_overview.content)
      this.drawFollowersChart(profile_overview.follower);
      this.drawFollowerGrowthChart(profile_overview.follower);
      this.drawTotalListedChart(profile_overview.follower);
      this.drawTotalListedGrowthChart(profile_overview.follower);
      this.drawProfileTweetChart(profile_overview.content);
      this.drawProfileActivitiesChart(profile_overview.content);
    });
  }

  drawFollowersChart(twitter_follower: any) {
    let highchartOptionsSeriesData = [];
    let highChartsCategories = [];

    Object.keys(twitter_follower).forEach((key) => {
      var follower = twitter_follower[key];
      highchartOptionsSeriesData.push(
        follower.total_followers
      )
      highChartsCategories.push(
        moment(follower.date).format("MMM DD")
      )
    })

    this.followerChartOptions = {
      series: [{
        name: "followers",
        data: highchartOptionsSeriesData
      }],
      title: "Total Followers",
      chart: {
        backgroundColor: null
      },
      xAxis: {
        categories: highChartsCategories
      },
      yAxis: {
        title: null,
        labels: {
          formatter: function(y_axis) {
            // console.log(y_axis.value);
            if (y_axis.value > 1000000) {
              return (y_axis.value/1000000) + "m"
            } else if (y_axis.value > 1000) {
              return (y_axis.value / 1000) + 'k'
            } else {
              return y_axis.value
            }
          }
        }
      }
    }
  }

drawFollowerGrowthChart(data: any) {
  let categories = [];
  let series_data = [];
  let cur_value = data[0].total_followers;

    Object.keys(data).forEach((key) => {
      var follower = data[key];
      let month = new Date(follower.date).toLocaleString('en-us', {month: 'short'});
      let growth = follower.total_followers - cur_value;
      categories.push(moment(follower.date).format('MMM DD'));
      series_data.push(growth);
      cur_value = follower.total_followers;
    })

    this.followerGrowthChartOptions = {
      series: [{
        name: "followers",
        data: series_data
      }],
      title: "Total Followers",
      chart: {
        backgroundColor: null,
        type: 'column'
      },
      xAxis: {
        categories: categories
      },
      yAxis: {
        title: null,
        labels: {
          formatter: function(y_axis) {
            // console.log(y_axis.value);
            if (y_axis.value > 1000) {
              return (y_axis.value / 1000) + 'k'
            } else {
              return y_axis.value
            }
          }
        }
      }
    }
}

drawTotalListedChart(data: any) {
  let categories = [];
  let series_data = []

  Object.keys(data).forEach((key) => {
    var follower = data[key];
    let month = new Date(follower.date).toLocaleString('en-us', {month: 'short'});
    categories.push(moment(follower.date).format("MMM DD"));
    series_data.push(follower.total_listed);
  })

  this.totalListedChartOptions = {
    series: [{
      name: "Total Listed",
      data: series_data
    }],
    title: "Total Listed",
    chart: {
      backgroundColor: null,
      type: 'line'
    },
    xAxis: {
      categories: categories,
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    yAxis: {
      title: null,
      labels: {
        formatter: function(y_axis) {
          // console.log(y_axis.value);
          if (y_axis.value > 1000) {
            return (y_axis.value / 1000) + 'k'
          } else {
            return y_axis.value
          }
        },
        style: {
          color: 'white'
        }
      }
    }
  }
}

drawTotalListedGrowthChart(data: any) {
  let categories = [];
  let series_data = [];
  let cur_value = data[0].total_listed;
  let values = []

    Object.keys(data).forEach((key) => {
      var follower = data[key];
      let growth = follower.total_listed - cur_value;
      categories.push(moment(follower.date).format("MMM DD"));
      series_data.push(growth);
      cur_value = follower.total_listed;
    });

    this.totalListedGrowthChartOptions = {
      title: "Total Followers",
      series: [
        {
          name: "Total Listed Growth",
          data: series_data
        }
      ],
      chart: {
        backgroundColor: null,
        type: 'column'
      },
      xAxis: {
        categories: categories,
        labels: {
          style: {
            color: 'white'
          }
        }
      },
      yAxis: {
        title: null,
        labels: {
          formatter: (y_axis) => {
            if (y_axis.value > 1000) {
              return (y_axis.value / 1000) + 'k'
            } else {
              return y_axis.value
            }
          },
          style: {
            color: 'white'
          }
        }
      }
    };
  }

  drawProfileTweetChart(data: any) {
    let categories = [];
    let series_data = []
    Object.keys(data).forEach((key) => {
      var content = data[key];
      let month = new Date(content.date).toLocaleString('en-us', {month: 'short'});
      categories.push(moment(content.date).format("MMM DD"));
      series_data.push(content.profile_tweets);
    });

    this.numberOfProfileTweetChartOptions = {
      title: "Number Of Profile Tweet",
      series: [
        {
          name: "Number Of Profile Tweet",
          data: series_data
        }
      ],
      chart: {
        backgroundColor: null,
        type: 'column'
      },
      xAxis: {
        categories: categories,
        labels: {
          style: {
            color: 'white'
          }
        }
      },
      yAxis: {
        title: null,
        labels: {
          formatter: (y_axis) => {
            if (y_axis.value > 1000) {
              return (y_axis.value / 1000) + 'k'
            } else {
              return y_axis.value
            }
          },
          style: {
            color: 'white'
          }
        }
      }
    };
  }

  drawProfileActivitiesChart(data: any) {
    let labels = [];
    let tweets = []
    let replies = []
    let retweets = []
    Object.keys(data).forEach((key) => {
      var content = data[key];
      let month = new Date(content.date).toLocaleString('en-us', {month: 'short'});
      labels.push(moment(content.date).format("MMM DDD"));
      tweets.push(content.profile_tweets);
      replies.push(content.total_replies);
      retweets.push(content.total_retweets);
    });

    console.log(tweets);
    console.log(replies);
    console.log(retweets);

    this.profileActivitiesChartOptions = {
      title: "Profile Activities",
      series: [
        {
          name: "Tweets",
          data: tweets
        },
        {
          name: "Replies",
          data: replies
        },
        {
          name: "Retweets",
          data: retweets
        }
      ],
      chart: {
        backgroundColor: null,
        type: 'column'
      },
      xAxis: {
        categories: labels,
        labels: {
          style: {
            color: 'white'
          }
        }
      },
      yAxis: {
        title: null,
        min: 0,
        stackLabels: {
          enabled: true
        },
        labels: {
          formatter: (y_axis) => {
            if (y_axis.value > 1000) {
              return (y_axis.value / 1000) + 'k'
            } else {
              return y_axis.value
            }
          },
          style: {
            color: 'white'
          }
        }
      },
      plotOptions: {
        column: {
          stacking: 'normal',
          dataLabels: {
              enabled: true,
              color: 'white'
          }
        }
      }
    };
  }

  drawEngagementRateChart(data: any) {
    let categories = [];
    let series_data = []
    Object.keys(data).forEach((key) => {
      var content = data[key];
      let month = new Date(content.date).toLocaleString('en-us', {month: 'short'});
      categories.push(moment(content.date).format("MMM DD"));
      let engagement = content.total_replies + content.total_retweets + content.total_likes
      let engagement_rate = (engagement / this.total_followers) * 100
      series_data.push(engagement_rate);
    });

    this.engagementRateChartOptions = {
      title: "Engagement Rate",
      series: [
        {
          name: "Engagement Rate",
          data: series_data
        }
      ],
      chart: {
        backgroundColor: null,
        type: 'line'
      },
      xAxis: {
        categories: categories,
        labels: {
          style: {
            color: 'white'
          }
        }
      },
      yAxis: {
        title: null,
        labels: {
          formatter: (y_axis) => {
            if (y_axis.value > 1000) {
              return (y_axis.value / 1000) + 'k'
            } else {
              return y_axis.value
            }
          },
          style: {
            color: 'white'
          }
        }
      },
      tooltip: {
        pointFormat: "Engagement Rate {point.y:.2f}"
      }
    };
  }

}

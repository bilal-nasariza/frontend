export class FollowerOverview {
    date: string;
    total_followers: number;
    total_following: number;
    total_listed: number;
}

export class ContentOverview {
    profile_tweets: number;
    profile_retweets: number;
    profile_replies: number;
}

export class EngagementOverview {
    date: string;
    mention: number;
    total_likes: number;
    total_replies: number;
    total_retweets: number;
}


export class TwitterOverview {
    follower: FollowerOverview[];
    content: ContentOverview[];
    engagement: EngagementOverview[];
}
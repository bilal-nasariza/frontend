import { TestBed, inject } from '@angular/core/testing';

import { TwitterOverviewService } from './twitter-overview.service';

describe('TwitterOverviewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TwitterOverviewService]
    });
  });

  it('should be created', inject([TwitterOverviewService], (service: TwitterOverviewService) => {
    expect(service).toBeTruthy();
  }));
});

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError as ObservableThrowError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { TwitterOverview } from './overview_models';

const api = 'http://localhost:5000';

@Injectable({
  providedIn: 'root'
})
export class TwitterOverviewService {

  constructor(private http: HttpClient) { }

  getTwitterOverview(twitter_id: number) {
    return this.http.post<TwitterOverview>(`${api}/analytics/tw/overview`, {"twitter_id": twitter_id})
    .pipe(
      tap(() => console.log(twitter_id))
    );
  }
}

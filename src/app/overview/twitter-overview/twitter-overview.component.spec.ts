import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwitterOverviewComponent } from './twitter-overview.component';

describe('TwitterOverviewComponent', () => {
  let component: TwitterOverviewComponent;
  let fixture: ComponentFixture<TwitterOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwitterOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwitterOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export class TwitterAccount {
    id: number;
    name: string;
    screen_name: number;
    total_followers: number;
    sum_of_profile_tweets: number;
}

export class FbAccount {
    name: string;
    total_fans: number;
    sum_of_page_posts: number;
}



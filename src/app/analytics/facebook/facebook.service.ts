import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError as ObservableThrowError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

export interface SearchFeed {
  q: string;
}

import { FacebookFeed } from './facebook_feed';

const api = 'http://localhost:5000';

@Injectable({
  providedIn: 'root'
})
export class FacebookService {

  constructor(private http: HttpClient) { }

  searchFeed(keyword: SearchFeed) {
    return this.http.post<FacebookFeed[]>(`${api}/analytics/fb/search`, keyword)
    .pipe(
      tap(() => console.log(keyword))
    );
  }
}

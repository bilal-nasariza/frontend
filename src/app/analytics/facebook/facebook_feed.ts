export class FacebookReaction {
    'haha': number;
    'like': number;
    'love': number;
    'wow': number;
    'sad': number;
    'angry': number;
    'total': number;
}

 export class FacebookFeed {
    id: number;
    created_time: string;
    reactions: FacebookReaction;
    message: string;
    sentiment: string;
    shares: number;
    comments: number;
    interactions: number;
}


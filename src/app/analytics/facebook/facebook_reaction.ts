export class FacebookReaction {
    'haha': number;
    'like': number;
    'love': number;
    'wow': number;
    'sad': number;
    'angry': 0
}
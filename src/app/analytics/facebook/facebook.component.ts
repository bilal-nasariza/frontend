import { Component, Input } from '@angular/core';
import { FacebookFeed } from './facebook_feed';

@Component({
  selector: 'app-facebook',
  templateUrl: './facebook.component.html',
  styleUrls: ['./facebook.component.scss']
})
export class FacebookComponent {

  @Input() feed: FacebookFeed;

}

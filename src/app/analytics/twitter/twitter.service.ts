import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError as ObservableThrowError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

export interface SearchTweet {
  q: string;
}

import { Tweet } from './tweet';

const api = 'http://localhost:5000';

@Injectable({
  providedIn: 'root'
})
export class TwitterService {

  constructor(private http: HttpClient) { }

  searchTweet(keyword: SearchTweet) {
    return this.http.post<Tweet[]>(`${api}/analytics/tw/search`, keyword)
    .pipe(
      tap(() => console.log(keyword))
    );
  }
}

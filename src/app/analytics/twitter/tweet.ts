import { TwitterUser } from './user';

export class Tweet {
    id: number;
    created_at: string;
    id_str: string;
    full_text: string;
    source: string;
    truncated: string;
    in_reply_to_status_id: string;
    in_reply_to_status_id_str: string;
    in_reply_to_user_id: number;
    in_reply_to_user_id_str: string;
    reply_count: number;
    retweet_count: number;
    favorite_count: number;
    lang: string;
    user: TwitterUser
}


import { Component, Input } from '@angular/core';
import { Tweet } from './tweet';

@Component({
  selector: 'app-twitter',
  templateUrl: './twitter.component.html',
  styleUrls: ['./twitter.component.scss']
})
export class TwitterComponent {

  @Input() tweet: Tweet;

}

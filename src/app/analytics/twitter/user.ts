export class TwitterUser {
    id: number;
    id_str: string;
    name: string;
    screen_name: string;
    location: string;
    url: string;
    description: string;
    protected: true;
    verified: boolean;
    followers_count: number;
    friends_count: number;
    listed_count: number;
    favourites_count: number;
    statuses_count: number;
    created_at: string;
    geo_enabled: boolean;
    lang: string;
    contributors_enabled: boolean;
    profil_backgroud_color: string;
    profile_background_image_url: string;
    profile_backgroud_image_url_https: string;
    profile_background_tile: boolean;
    profile_banner_url: string;
    profile_image_url: string;
}
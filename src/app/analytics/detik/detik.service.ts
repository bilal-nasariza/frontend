import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError as ObservableThrowError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

export interface SearchDetik {
  q: string;
}

import { DetikResult } from './detik';

const api = 'http://localhost:5000';

@Injectable({
  providedIn: 'root'
})
export class DetikService {

  constructor(private http: HttpClient) { }

  searchDetik(keyword: SearchDetik) {
    return this.http.post<DetikResult[]>(`${api}/analytics/dtk/search`, keyword)
    .pipe(
      tap(() => console.log(keyword))
    );
  }
}

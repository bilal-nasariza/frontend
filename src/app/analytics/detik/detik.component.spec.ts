import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetikComponent } from './detik.component';

describe('DetikComponent', () => {
  let component: DetikComponent;
  let fixture: ComponentFixture<DetikComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetikComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetikComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

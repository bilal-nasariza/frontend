
export class Comments {
    comment_count: number;
    comments: [];
}

export class DetikResult {
    comments: Comments;
    date: string;
    link: string;
    sentiment: string;
    text: string;
    title: string;
}

import { Component, Input } from '@angular/core';
import { DetikResult } from './detik';

@Component({
  selector: 'app-detik',
  templateUrl: './detik.component.html',
  styleUrls: ['./detik.component.scss']
})
export class DetikComponent  {

  @Input() result: DetikResult;

}

import { Component, OnInit } from '@angular/core';
import { Tweet } from './twitter/tweet';
import { TwitterService } from './twitter/twitter.service'
import { FacebookFeed } from './facebook/facebook_feed';
import { FacebookService } from './facebook/facebook.service';
import { KompasService } from './kompas/kompas.service';
import { KompasResult } from './kompas/kompas';
import { DetikService } from './detik/detik.service';
import { DetikResult } from './detik/detik';
import { finalize } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {

  tweets: Tweet[];
  loading: boolean;
  fb_feeds: FacebookFeed[];
  kompas_results: KompasResult[];
  detik_results: DetikResult[];

  constructor(
    private twiterService: TwitterService, 
    private fb_service: FacebookService, 
    private kompas_service: KompasService,
    private detik_service: DetikService) { }

  ngOnInit() {
  }

  keyword = new FormControl('');

  getData() {
    console.log(this.keyword.value);
    this.analyzeKeyword(this.keyword.value);
  }

  analyzeKeyword(keyword: string) {
    this.loading = true;
    this.twiterService.searchTweet({'q': keyword})
    .pipe(finalize(() => (this.loading = false)))
    .subscribe(tweets => (
      this.tweets = tweets
      ));

    this.loading = true;
    this.fb_service.searchFeed({'q': keyword})
    .pipe(finalize(() => (this.loading = false)))
    .subscribe(feeds => (
      this.fb_feeds = feeds
    ));

    this.loading = true;
    this.kompas_service.searchKompas({'q': keyword})
    .pipe(finalize(() => (this.loading = false)))
    .subscribe(results => (
      this.kompas_results = results
    ));

    this.loading = true;
    this.detik_service.searchDetik({'q': keyword})
    .pipe(finalize(() => (this.loading = false)))
    .subscribe(results => (
      this.detik_results = results
    ));

  }

}

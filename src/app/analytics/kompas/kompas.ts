
export class Comment {
    author: string;
    comment: string;
    sentiment: string;
}

export class KompasResult {
    comments: Comment[];
    content_author: string;
    editor: string;
    sentiment: string;
    snippet: string;
    tag: string;
    title: string;
    comment_count: number;
}

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError as ObservableThrowError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

export interface SearchKompas {
  q: string;
}

import { KompasResult } from './kompas';

const api = 'http://localhost:5000';

@Injectable({
  providedIn: 'root'
})
export class KompasService {

  constructor(private http: HttpClient) { }

  searchKompas(keyword: SearchKompas) {
    return this.http.post<KompasResult[]>(`${api}/analytics/ks/search`, keyword)
    .pipe(
      tap(() => console.log(keyword))
    );
  }
}

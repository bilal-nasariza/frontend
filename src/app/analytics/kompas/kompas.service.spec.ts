import { TestBed, inject } from '@angular/core/testing';

import { KompasService } from './kompas.service';

describe('KompasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KompasService]
    });
  });

  it('should be created', inject([KompasService], (service: KompasService) => {
    expect(service).toBeTruthy();
  }));
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KompasComponent } from './kompas.component';

describe('KompasComponent', () => {
  let component: KompasComponent;
  let fixture: ComponentFixture<KompasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KompasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KompasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

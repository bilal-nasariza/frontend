import { Component, Input } from '@angular/core';
import { KompasResult } from './kompas';

@Component({
  selector: 'app-kompas',
  templateUrl: './kompas.component.html',
  styleUrls: ['./kompas.component.scss']
})
export class KompasComponent {

  @Input() result: KompasResult;

}
